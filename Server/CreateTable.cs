﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevComponents.DotNetBar;
using Maticsoft.Model;
using Maticsoft.BLL;

namespace Server
{
    public partial class CreateTable : Office2007Form
    {
        public CreateTable()
        {
            InitializeComponent();
        }

        public DataTable dt = new DataTable();
        public Boolean bol = false;
        //窗体加载事件
        private void SetColumns_Load(object sender, EventArgs e)
        {
            textBoxX1.Focus();
        }

        //保存数据表
        private void buttonX1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!textBoxX1.Text.Trim().Equals(""))
                {
                    if (frmMain.SelectTable(textBoxX1.Text.Trim()))
                    {
                        frmMain.CreateTable(textBoxX1.Text.Trim(),dt);
                        if (bol) FrmMain.tableName = textBoxX1.Text.Trim();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("数据表已存在", "提示");
                        textBoxX1.Text = "";
                        textBoxX1.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("数据表不能为空", "提示");
                    textBoxX1.Text = "";
                    textBoxX1.Focus();
                }
            }
            catch { }
        }
    }
}
